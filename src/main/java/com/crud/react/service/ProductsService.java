package com.crud.react.service;

import com.crud.react.model.Products;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface ProductsService {
    Products addProducts(Products products, MultipartFile multipartFile);

    Page<Products> getAll(int page, String search);

    Products getProduct(Integer id);

    Products editProduct(Integer id, Products products, MultipartFile multipartFile);

    Map<String, Boolean> deleteProduct(Integer id);
}
