package com.crud.react.service;


import com.crud.react.DTO.LoginDto;
import com.crud.react.model.Users;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface UserService {

    Map<String, Object> login(LoginDto loginDto);

    Users addUsers(Users users, MultipartFile multipartFile);

    List<Users> getAll();

    Users getUsersById(Integer id);


    Users editUsers(Integer id , Users users, MultipartFile multipartFile);

    Map<String, Boolean> deleteUsers(Integer id);
}
