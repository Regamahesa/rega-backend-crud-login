package com.crud.react.service;

import com.crud.react.DTO.CartDto;
import com.crud.react.model.Cart;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface CartServise {

    Cart addCart(CartDto cart);


    Page<Cart> getAllCart(int page, Long users_id);

    Cart getCart(Integer id);

    Cart editCart(Integer id, CartDto cart);

    Map<String, Object> deleteCart(Integer id);

    Map<String, Boolean> checkOutByUser(int userId);
}
