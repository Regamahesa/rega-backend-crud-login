package com.crud.react.controller;

import com.crud.react.DTO.ProductDto;
import com.crud.react.model.Products;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.ProductsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductsService productsService;

    @Autowired
    ModelMapper modelMapper;

    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<Products> addProduct(ProductDto productDto, @RequestPart("file") MultipartFile multipartFile ) {
        return ResponseHelper.ok(productsService.addProducts(modelMapper.map(productDto , Products.class) ,multipartFile));
    }
    @GetMapping("/all")
    public CommonResponse<Page<Products>> getAll(@RequestParam(required = false) int page, @RequestParam(required = false) String search) {
        return ResponseHelper.ok(productsService.   getAll(page,search  == null ? "" : search));
    }
    @GetMapping("/{id}")
    public CommonResponse<Products> getById(@PathVariable("id") Integer id) {
        return ResponseHelper.ok(productsService.getProduct(id));
    }
    @PutMapping(value = "/{id}", consumes = "multipart/form-data")
    public CommonResponse<Products> editProduct(ProductDto productDto,@PathVariable("id") Integer id,@RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(productsService.editProduct(id, modelMapper.map(productDto, Products.class), multipartFile));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteProduct(@PathVariable("id") Integer id){
        return ResponseHelper.ok(productsService.deleteProduct(id));
    }
}
