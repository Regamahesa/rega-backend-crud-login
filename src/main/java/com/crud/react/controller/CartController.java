package com.crud.react.controller;

import com.crud.react.DTO.CartDto;
import com.crud.react.model.Cart;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.CartServise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartServise cartServise;

    @PostMapping
    public CommonResponse<Cart> addCart(@RequestBody CartDto cart) {
        return ResponseHelper.ok( cartServise.addCart(cart));
    }

    @GetMapping
    public CommonResponse<Page<Cart>> getAllCart(@RequestParam int page, Long users_id) {
        return ResponseHelper.ok(cartServise.getAllCart(page, users_id));
    }

    @PutMapping("/{id}")
    public CommonResponse<Cart> editCart(@PathVariable("id") int id, @RequestBody CartDto cart) {
        return ResponseHelper.ok(cartServise.editCart(id, cart));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Object>> deleteCart(@PathVariable("id") int id) {
        return ResponseHelper.ok( cartServise.deleteCart(id));
    }

    @DeleteMapping
    public CommonResponse<Map<String, Boolean>> checkOutByUser(@RequestParam int userId){
        return ResponseHelper.ok(cartServise.checkOutByUser(userId));
    }

}
