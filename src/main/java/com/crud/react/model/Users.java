package com.crud.react.model;

import com.crud.react.enumated.UserEnum;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "nama_kamu")
    private String namaKamu;

    @Column(name = "alamat")
    private String alamat;

    @Lob
    @Column(name = "foto_profile")
    private String fotoProfile;

    @Column(name = "no_telepon")
    private String noTelepon;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserEnum role;

    public Users(String email, String password, String namaKamu, String alamat, String fotoProfile, String noTelepon, UserEnum role) {
        this.email = email;
        this.password = password;
        this.namaKamu = namaKamu;
        this.alamat = alamat;
        this.fotoProfile = fotoProfile;
        this.noTelepon = noTelepon;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserEnum getRole() {
        return role;
    }

    public void setRole(UserEnum role) {
        this.role = role;
    }

    public String getNamaKamu() {
        return namaKamu;
    }

    public void setNamaKamu(String namaKamu) {
        this.namaKamu = namaKamu;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getFotoProfile() {
        return fotoProfile;
    }

    public void setFotoProfile(String fotoProfile) {
        this.fotoProfile = fotoProfile;
    }

    public String getNoTelepon() {
        return noTelepon;
    }

    public void setNoTelepon(String noTelepon) {
        this.noTelepon = noTelepon;
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", namaKamu='" + namaKamu + '\'' +
                ", alamat='" + alamat + '\'' +
                ", fotoProfile='" + fotoProfile + '\'' +
                ", noTelepon=" + noTelepon +
                ", role=" + role +
                '}';
    }

    public Users() {
    }

    public Users(String email, String password, UserEnum role) {
        this.email = email;
        this.password = password;
        this.role = role;
    }
}
