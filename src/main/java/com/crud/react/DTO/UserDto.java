package com.crud.react.DTO;

import com.crud.react.enumated.UserEnum;

public class UserDto {
    private String email;
    private String password;
    private String role;

    private String namaKamu;

    private String alamat;

    private String noTelepon;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNamaKamu() {
        return namaKamu;
    }

    public void setNamaKamu(String namaKamu) {
        this.namaKamu = namaKamu;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoTelepon() {
        return noTelepon;
    }

    public void setNoTelepon(String noTelepon) {
        this.noTelepon = noTelepon;
    }
}
