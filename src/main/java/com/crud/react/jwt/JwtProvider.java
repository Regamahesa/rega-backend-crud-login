package com.crud.react.jwt;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.TemporaryToken;
import com.crud.react.model.Users;
import com.crud.react.repository.TemporaryRepository;
import com.crud.react.repository.UsersRepository;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {

private static String secretKey = "belajar spring";
private static Integer expired = 900000;
@Autowired
private TemporaryRepository temporaryRepository;
@Autowired
private UsersRepository usersRepository;

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("", "");
        Users user = usersRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User not found generate token"));
        var chekingToken = temporaryRepository.findById(user.getId());
        if (chekingToken.isPresent()) temporaryRepository.deleteById(chekingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(user.getId());
        temporaryRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject(String token) {
        return temporaryRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("token error parse"));
    }
    public boolean checkingTokenJwt(String token) {
    TemporaryToken tokenExist = temporaryRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token Expired");
            return false;
        }
        return true;
    }
}
