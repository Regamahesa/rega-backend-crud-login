package com.crud.react.service_impl;

import com.crud.react.DTO.CartDto;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Cart;
import com.crud.react.model.Products;
import com.crud.react.repository.CartRepository;
import com.crud.react.repository.ProductsRepository;
import com.crud.react.repository.UsersRepository;
import com.crud.react.service.CartServise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CartServiceImpl implements CartServise {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private CartRepository cartRepository;

    @Override
    public Cart addCart(CartDto cart) {
        Cart create = new Cart();
        Products products = productsRepository.findById(cart.getProductId()).orElseThrow(() -> new NotFoundException("Product id tidak ditemukan"));
        create.setQty(cart.getQty());
        create.setTotalHarga(products.getHarga() * cart.getQty());
        create.setUserId(usersRepository.findById(cart.getUserId()).orElseThrow(() -> new NotFoundException("user id tidak ditemukan")));
        create.setProductsId(products);
        return cartRepository.save(create);
    }

    @Override
    public Page<Cart> getAllCart(int page, Long users_id) {
        Pageable pageable = PageRequest.of(page, 5);
        return cartRepository.findByUserId(users_id, pageable);
    }

    @Override
    public Cart getCart(Integer id) {
        return null;
    }

    @Override
    public Cart editCart(Integer id, CartDto cart) {
        Cart data = cartRepository.findById(id).orElseThrow(() -> new NotFoundException("id tidak ditemukan"));
        data.setUserId(usersRepository.findById(cart.getUserId()).orElseThrow(() -> new NotFoundException("user id tidak ditemukan")));
        data.setProductsId(productsRepository.findById(cart.getProductId()).orElseThrow(() -> new NotFoundException("user id tidak ditemukan")));
        return cartRepository.save(data);
    }

    @Override
    public Map<String, Object> deleteCart(Integer id) {
        cartRepository.deleteById(id);
        Map<String, Object> obj = new HashMap<>();
        obj.put("DELETED", true);
        return obj;
    }

    @Override
    public Map<String, Boolean> checkOutByUser(int userId) {
        List<Cart> listCart = cartRepository.findAllByUserId(userId);
        cartRepository.deleteAll(listCart);
        Map<String, Boolean> obj = new HashMap<>();
        obj.put("deleted", Boolean.TRUE);
        return obj;
    }
}


