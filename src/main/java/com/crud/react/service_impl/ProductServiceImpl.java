package com.crud.react.service_impl;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Products;
import com.crud.react.repository.ProductsRepository;
import com.crud.react.service.ProductsService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductsService {

    @Autowired
    ProductsRepository productsRepository;

    //FireBase
    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/upload-image-examle.appspot.com/o/%s?alt=media";

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtention(multipartFile.getOriginalFilename());
            File file = convertToFile (multipartFile, fileName);
            var RESPONSE_URL =uploadFile( file, fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error Upload File");
        }
    }
    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }
    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("upload-image-examle.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }
    @Override
    public Products addProducts(Products products, MultipartFile multipartFile) {
        String img = imageConverter(multipartFile);
        Products addProducts = new Products(products.getNama(), products.getDeskripsi() , img, products.getHarga());
        return productsRepository.save(addProducts);
    }

    private String getExtention(String filename) {
        return filename.split("\\.")[0];
    }

//    End FireBase


    @Override
    public Page<Products> getAll(int page , String search) {
        Pageable pages = PageRequest.of(page, 5);
        return productsRepository.findBySearch(search, pages);
    }

    @Override
    public Products getProduct(Integer id) {
        return productsRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
    }

    @Transactional
    @Override
    public Products editProduct(Integer id, Products products, MultipartFile multipartFile) {
        Products update = productsRepository.findById(id).orElseThrow(() -> new NotFoundException(("Tidak Ada")));
        String img = imageConverter(multipartFile);
        update.setImg(img);
        update.setNama(products.getNama());
        update.setDeskripsi(products.getDeskripsi());
        update.setHarga(products.getHarga());
        return productsRepository.save(update);
    }

    @Override
    public Map<String, Boolean> deleteProduct(Integer id) {
        try {
            productsRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
