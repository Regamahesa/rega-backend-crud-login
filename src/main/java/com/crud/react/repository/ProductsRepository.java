package com.crud.react.repository;

import com.crud.react.model.Products;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductsRepository extends JpaRepository<Products,Integer> {

    @Query(value = "select * from products where nama LIKE CONCAT ('%', ?1, '%')", nativeQuery = true)
    Page<Products> findBySearch(String search, Pageable pageable);

}
