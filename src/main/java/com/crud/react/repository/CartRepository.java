package com.crud.react.repository;

import com.crud.react.model.Cart;
import org.apache.catalina.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository  extends JpaRepository<Cart , Integer> {
    @Query(value = "select * from cart where user_id = :users_id", nativeQuery = true)
    Page<Cart> findByUserId(Long users_id, Pageable pageable);

    @Query(value = "SELECT * FROM cart WHERE user_id = :userId", nativeQuery = true)
    List<Cart> findAllByUserId(int userId);

}
